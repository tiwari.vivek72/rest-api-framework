#Author: tiwari.vivek23011988@gmail.com

@Regression

Feature: Run automated test suite to check that the request fails when incorrect api call is made

Background:
Given Rates <URL> for Latest Foreign Exchange rates
|"API_BASIC_URL"|"FEATURE_TWO"|
|"API_BASIC_URL"|"FEATURE_TWO"|

Scenario Outline: Check Latest Foreign Exchange rate url and response status
When The user provides an incorrect <url>
Then The request should fail with <status>
Examples:
|url|status|
|"API_BASIC_URL"|"RESPONSE_STATUS"|

Scenario Outline: Check Latest Foreign Exchange rate response status line
When The user hits a <url>
Then The response status line should be <statusLine>
Examples:
|url|statusLine|
|"API_BASIC_URL"|"RESPONSE_STATUS_LINE"|

Scenario Outline: Check Latest Foreign Exchange rate response format
When The user hits a <url>
Then The response should be in <statusFormat> format
Examples:
|url|statusFormat|
|"API_BASIC_URL"|"RESPONSE_FORMAT"|

Scenario Outline: Check Latest Foreign Exchange rate response header
When The user hits a <url>
Then The response should contain <header> with content <headerValue>
Examples:
|url|header|headerValue|
|"API_BASIC_URL"|"RESPONSE_HEADER"|"RESPONSE_HEADER_CONTENT"|