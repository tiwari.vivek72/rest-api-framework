#Author: tiwari.vivek23011988@gmail.com

@Smoke

Feature: Run automated test suite to test api-response for Latest Foreign Exchange rates

Background:
Given Rates <API> for Latest Foreign Exchange rates
|"API_BASIC_URL"|"FEATURE_ONE"|
|"API_BASIC_URL_WITH_PARAM"|"FEATURE_ONE"|
|"API_BASIC_URL_WITH_PARAM"|"FEATURE_ONE"|
|"API_BASIC_URL_WITH_PARAM"|"FEATURE_ONE"|
|"API_BASIC_URL"|"FEATURE_ONE"|
|"API_BASIC_URL_WITH_PARAM"|"FEATURE_ONE"|
|"API_BASIC_URL_WITH_PARAM"|"FEATURE_ONE"|
|"API_BASIC_URL_WITH_PARAM"|"FEATURE_ONE"|

Scenario Outline: Check Latest Foreign Exchange rate api and response status
When The <api> is available
Then Check success <status> of the response
Examples:
|api|status|
|"API_BASIC_URL"|"RESPONSE_STATUS"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_STATUS_WITH_PARAM"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_STATUS_WITH_PARAM"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_STATUS_WITH_PARAM"|

Scenario Outline: Check Latest Foreign Exchange rate api and response schema
When The <api> is available
Then Check the response <body>
Examples:
|api|body|
|"API_BASIC_URL"|"RESPONSE_SCHEMA"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_SCHEMA_WITH_PARAM"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_SCHEMA_WITH_PARAM"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_SCHEMA_WITH_PARAM"|

Scenario Outline: Check Date Specific Foreign Exchange rate api and response status
When The <api> is available
Then Check success <status> of the response
Examples:
|api|status|
|"API_BASIC_URL"|"RESPONSE_STATUS"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_STATUS_WITH_PARAM"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_STATUS_WITH_PARAM"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_STATUS_WITH_PARAM"|

Scenario Outline: Check Date Specific Foreign Exchange rate api and response schema
When The <api> is available
Then Check the response <body>
Examples:
|api|body|
|"API_BASIC_URL"|"RESPONSE_SCHEMA"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_SCHEMA_WITH_PARAM"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_SCHEMA_WITH_PARAM"|
|"API_BASIC_URL_WITH_PARAM"|"RESPONSE_SCHEMA_WITH_PARAM"|