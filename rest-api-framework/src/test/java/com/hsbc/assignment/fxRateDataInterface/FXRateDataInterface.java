package com.hsbc.assignment.fxRateDataInterface;

/**
 * 
 * @author Vivek Tiwari
 * 
 * Content - Abstract Methods to test Rest-Api Calls
 * 
 *
 */

public interface FXRateDataInterface {

	void testApiUrlIsNotNull(String url, int index);

	void testApiResponse(String url);

	void testApiResponseCode(String responseCode);

	void testApiResponseSchema(String responseBody);

	void checkAndExecuteScenario(String featureName, int i);

	void checkApiResponseStatusLine(String responseStatusLine);

}
