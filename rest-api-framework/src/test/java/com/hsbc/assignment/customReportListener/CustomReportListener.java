package com.hsbc.assignment.customReportListener;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.gherkin.model.Given;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.cucumber.plugin.EventListener;
import io.cucumber.plugin.event.EventPublisher;
import io.cucumber.plugin.event.HookTestStep;
import io.cucumber.plugin.event.PickleStepTestStep;
import io.cucumber.plugin.event.TestCaseStarted;
import io.cucumber.plugin.event.TestRunFinished;
import io.cucumber.plugin.event.TestRunStarted;
import io.cucumber.plugin.event.TestSourceRead;
import io.cucumber.plugin.event.TestStepFinished;
import io.cucumber.plugin.event.TestStepStarted;


public class CustomReportListener implements EventListener {

	private static Logger logger = Logger.getLogger(CustomReportListener.class);
	
	private ExtentSparkReporter spark;
	
	private ExtentReports extent;
	
	Map<String, ExtentTest> feature = new HashMap<String, ExtentTest>();
	
	ExtentTest scenario;
	ExtentTest step;
	
	@Override
	public void setEventPublisher(EventPublisher publisher) {

		try {
			
			publisher.registerHandlerFor(TestRunStarted.class, this::runStarted);
			
			publisher.registerHandlerFor(TestRunFinished.class, this::runFinished);

			publisher.registerHandlerFor(TestSourceRead.class, this::featureRead);

			publisher.registerHandlerFor(TestCaseStarted.class, this::scenarioStarted);

			publisher.registerHandlerFor(TestStepStarted.class, this::stepStarted);

			publisher.registerHandlerFor(TestStepFinished.class, this::stepFinished);

			
		}
		catch(Exception e) {
			
			logger.error("Received exception in setEventPublisher function:", e);

		}
	}

	public void runStarted(TestRunStarted event) {
		
		try {
			
			spark = new ExtentSparkReporter("./ExtentResultsReport.html");
			
			extent = new ExtentReports();
			
			spark.config().setTheme(Theme.STANDARD);
			
			extent.attachReporter(spark);
		
		}catch(Exception e) {
			
			logger.error("Received exception in runStarted function:", e);
		}
		
	}
	
	public void runFinished(TestRunFinished event) {
		
		try {
			
			extent.flush();
			
		}catch(Exception e){
			
			logger.error("Received exception in runFinished function:", e);

		}
	}
	
	private void featureRead(TestSourceRead event) {
		
		try {
			
			String featureSource = event.getUri().toString();
			String featureName = featureSource.split(".*/")[1];
			if(feature.get(featureSource)==null)
				feature.putIfAbsent(featureSource, extent.createTest(featureName));
		
		}catch(Exception e) {
			
			logger.error("Received exception in featureRead function:", e);

		}
		
	}
	
	private void scenarioStarted(TestCaseStarted event) {
		
		try {
			
			String featureName = event.getTestCase().getUri().toString();
			
			scenario = feature.get(featureName).createNode(event.getTestCase().getName());
			
		} catch(Exception e) {
			
			logger.error("Received exception in scenarioStarted function:", e);

		}
	}
	
	private void stepStarted(TestStepStarted event) {
		
		try {
			
			String stepName = " ";
			
			String keyword = "Triggered the hook :";
			
			if(event.getTestStep() instanceof PickleStepTestStep)
			{
				
				PickleStepTestStep steps = (PickleStepTestStep) event.getTestStep();
				
				stepName = steps.getStep().getText();
				
				keyword = steps.getStep().getKeyWord();
				
			}else {
				
				HookTestStep hook = (HookTestStep) event.getTestStep();
				
				stepName = hook.getHookType().name();
			}
			
			step = scenario.createNode(Given.class, keyword + " " + stepName);
			
		} catch(Exception e) {
			
			logger.error("Received exception in stepStarted function:", e);

		}
	}
	
	private void stepFinished(TestStepFinished event) {
		
		try {
			
			if(event.getResult().getStatus().toString() == "PASSED") {
				
				step.log(Status.PASS, "This step passed");
			}
			else if(event.getResult().getStatus().toString() == "SKIPPED") {
				
				step.log(Status.SKIP, "This step was skipped");
			}
			else {
				
				step.log(Status.FAIL, "This step failed");
			}
		} catch(Exception e) {
			
			logger.error("Received exception in stepFinished function:", e);

		}
	}
}
