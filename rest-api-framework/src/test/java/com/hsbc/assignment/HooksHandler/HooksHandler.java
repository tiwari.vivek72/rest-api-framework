package com.hsbc.assignment.HooksHandler;

import org.apache.log4j.Logger;

/**
 * 
 * @author Vivek Tiwari
 * 
 * Content - Checks if the hooks are called multiple times at once.
 *
 */


public class HooksHandler {
	
		 /**
		  * logger - To log information and errors
		  * 
		  */
	
		  private static final Logger logger = Logger.getLogger(HooksHandler.class);
	
		  private static HooksHandler instance;

		  private int resultOne = 0;
		  
		  private double resultTwo = 0;

		  private boolean isSetup;

		  private HooksHandler() {
		  }
		  
		  /**
		   * Synchronized setUp Method
		   * 
		   * @return HooksHandler -- If instance is null, else throws runtime exception.
		   */
		  
		  public static synchronized HooksHandler setup() {
			  
			try {
				
			
		    if (instance == null)
		        	instance = new HooksHandler(); 

		    if (instance.isSetup) {
		        throw new RuntimeException("I have already been set up");
		    }

		    instance.isSetup = true;

		    return instance;
		    
			}catch(Exception e) {
				
				logger.error("Received exception in setup function:", e);
			}
			
			return null;
		  }

		  /**
		   * 
		   * cleanUp Method - Throws runtime exception, if the instance setup is not in place.
		   * 
		   */
		  
		  public void cleanUp() {
			  
			try {
				
			
		    if (!isSetup) {
		    	
		        throw new RuntimeException("I wasn't set up in the first place");
		    }

		    isSetup = false;
		    
			}catch(Exception e) {
				
				logger.error("Received exception in cleanUp function:", e);
			}
			

		  }

		  
		  /**
		   * Add method - to perform add operation on final fields
		   * 
		   * @param arg1 - Final Argument 1
		   * @param arg2 - Final Argument 2
		   */
		  
		  public void add(final int arg1, final int arg2) {
			  
			try {
				
		    if (!isSetup) {
		        throw new RuntimeException("I haven't been set up, no calculation for you!");
		    }

		    resultOne = arg1 + arg2;
		    
			}catch(Exception e) {
				
				logger.error("Received exception in add function:", e);
			}
		  }

		  
		  /**
		   * 
		   * @return result of add method
		   */
		  
		  public int getResultOne() {
			  
			try {
				
			
		    if (!isSetup) {
		        throw new RuntimeException("I haven't been set up, no calculation for you!");
		    }

		    return resultOne;
		    
			}catch(Exception e) {
				
				logger.error("Received exception in getResult function:", e);
			}
			
			return 0;
		  }

		  /**
		   * Subtract method - to perform subtract operation on final fields
		   * 
		   * @param arg1 - Final Argument 1
		   * @param arg2 - Final Argument 2
		   */
		  
		  public void subtract(final int arg1, final int arg2) {
			  
			try {
				
		    if (!isSetup) {
		        throw new RuntimeException("I haven't been set up, no calculation for you!");
		    }

		    resultTwo = arg2 - arg1;
		    
			}catch(Exception e) {
				
				logger.error("Received exception in subtract function:", e);
			}
		  }

		  /**
		   * 
		   * @return result of subtract method
		   */
		  
		  public double getresultTwo() {
			 
			try {
				
		    if (!isSetup) {
		        throw new RuntimeException("I haven't been set up, no calculation for you!");
		    }

		    return resultTwo;
		    
			}catch(Exception e) {
				
				logger.error("Received exception in getresultTwo function:", e);
			}
			
			return 0.0d;
		  }
}
