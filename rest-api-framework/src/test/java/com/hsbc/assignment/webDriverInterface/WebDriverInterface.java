package com.hsbc.assignment.webDriverInterface;

import org.openqa.selenium.WebDriver;

/**
 * 
 * @author Vivek Tiwari
 *
 *@Content - Abstract methods for Web-Driver Implementation
 *
 * Note - Under Construction
 * 
 */
public interface WebDriverInterface {

	WebDriver getWebDriverType(String browserType);
}
