package com.hsbc.assignment.stepDefinitions;

import java.util.List;

import org.apache.log4j.Logger;

import com.hsbc.assignment.HooksHandler.HooksHandler;
import com.hsbc.assignment.utilities.FXRateDataUtility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * 
 * @author Vivek Tiwari
 * 
 * @Content - Cucumber steps for feature file
 * 
 * @Before hook and @After hooks for demo
 *
 */
public class LatestFXRateDataAdditionalScenariosStepDefinition {

/**
 * final logger object to log information and errors
 */
private final Logger logger = Logger.getLogger(LatestFXRateDataAdditionalScenariosStepDefinition.class);
	
	/**
	 * FXRateDataUtility Utility class object.
	 * 
	 * Calls res-assured functions and processing methods
	 */

	private FXRateDataUtility fxRateDataUtility = new FXRateDataUtility();
	
	/**
	 * HooksHandler class object to check if one instance of @Before and @After hooks run at once
	 */
	
	private HooksHandler hooksHandler;
	
	/**
	 * Function tests Before hook with tag mentioned for the corresponding feature file
	 * 
	 * Runs before scenario
	 */
	
	@Before("@Regression")
	public void setUp() {
		
		hooksHandler = HooksHandler.setup();
		
		logger.info("Called Before Scenario Hook");
	}
	
	/**
	 * Function checks if API key has associated values in the data file
	 * 
	 * @param dataTable - table storing set of parameters.
	 */
	
	@Given("^Rates <URL> for Latest Foreign Exchange rates$")
	public void checkIncorrectApiURLIsNotNull(DataTable dataTable) {
	    
		  try {
		 
			  List<List<String>> dataTableElements = dataTable.asList(List.class);
				
			  for(int i=0;i<dataTableElements.size();i++)
			   {
				   
				  fxRateDataUtility.checkAndExecuteScenario(dataTableElements.get(i).get(1).substring(1, dataTableElements.get(i).get(1).length()-1),dataTableElements.size()-1);
				   
				  fxRateDataUtility.testApiUrlIsNotNull(dataTableElements.get(i).get(0).substring(1, dataTableElements.get(i).get(0).length()-1),i);
			   
			   }
		    
		  } catch(Exception e) {
			  
			  logger.error("Received exception in checkApiURLIsNotNull function:", e);
		  }
		  
	}
	
	/**
	 * Function checks API response for incorrect/in-complete URL
	 * 
	 * @param apiUrl - API URL Key.
	 */
	@When("^The user provides an incorrect \\\"([^\\\"]*)\\\"$")
	public void checkIncorrectUrlResponse(String apiUrl) {
	    
		try {
	   
			fxRateDataUtility.testApiResponse(apiUrl);
	   
		} catch(Exception e) {
			  
			  logger.error("Received exception in checkIncorrectUrlResponse function:", e);
		}
	}

	/**
	 * Function checks API response code for incorrect/in-complete URL
	 * 
	 * @param responseCode - API response code key.
	 */
	
	@Then("^The request should fail with \\\"([^\\\"]*)\\\"$")
	public void checkRequestFailureStatusCodeForIncorrectUrl(String responseCode) {
	   
		try {
			   
			fxRateDataUtility.testApiResponseCode(responseCode);
		
		} catch(Exception e) {
			  
			  logger.error("Received exception in checkRequestFailureStatusCodeForIncorrectUrl function:", e);
		}
	}
	
	/**
	 * Function checks API response
	 * 
	 * @param url - API URL key
	 */
	
	@When("The user hits a {string}")
	public void theUserHitsAUrl(String url) {
   
		fxRateDataUtility.testApiResponse(url);
	
	}

	/**
	 * Function checks API response status-line
	 * 
	 * @param statusLine - API response status line key.
	 */
	
	@Then("The response status line should be {string}")
	public void checkResponseStatusLine(String statusLineKey) {
   
		fxRateDataUtility.checkApiResponseStatusLine(statusLineKey);
	}

	/**
	 * Function checks API response content-type
	 * 
	 * @param responseFormat - API response content type.
	 */
	
	@Then("The response should be in {string} format")
	public void checkResponseFormat(String responseFormat) {
	  
		fxRateDataUtility.checkApiResponseContentType(responseFormat);
	}

	/**
	 * Function checks API response header
	 * 
	 * @param responseHeader - API response header.
	 */
	
	@Then("The response should contain {string} with content {string}")
	public void checkResponseHeader(String responseHeader, String responseHeaderContent) {
	   
		fxRateDataUtility.checkApiResponseHeader(responseHeader, responseHeaderContent);
	}
	
	
	/**
	 * Function tests After hook with tag mentioned for the corresponding feature file
	 * 
	 * Runs after scenario
	 */
	
	@After("@Regression")
	public void tearDown() {
		
		hooksHandler.cleanUp();
		
		logger.info("Called After Scenario Hook");
	}
	
}
