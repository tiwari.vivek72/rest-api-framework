package com.hsbc.assignment.stepDefinitions;

import java.util.List;

import org.apache.log4j.Logger;

import com.hsbc.assignment.HooksHandler.HooksHandler;
import com.hsbc.assignment.utilities.FXRateDataUtility;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * 
 * @author Vivek Tiwari
 * 
 * @Content - Cucumber steps for feature file
 * 
 * @Before hook and @After hooks for demo
 *
 */

public class LatestFXRateDataStepDefinition {
	
	/**
	 * final logger object to log information and errors
	 */
	
	private final Logger logger = Logger.getLogger(LatestFXRateDataStepDefinition.class);
	
	/**
	 * FXRateDataUtility Utility class object.
	 * 
	 * Calls rest-assured functions and processing methods
	 */
	
	private FXRateDataUtility fxRateDataUtility = new FXRateDataUtility();
	
	/**
	 * HooksHandler class object to check if one instance of @Before and @After hooks run at once
	 */
	
	private HooksHandler hooksHandler;
	
	/**
	 * Function tests Before hook with tag mentioned for the corresponding feature file
	 * 
	 * Runs before scenario
	 */
	
	@Before("@Smoke")
	public void setUp() {
		
		hooksHandler = HooksHandler.setup();
		
		logger.info("Called Before Scenario Hook");
	}
	
	/**
	 * Function checks if API key has associated values in the data file
	 * 
	 * @param dataTable - table storing set of parameters.
	 */
	
	@Given("^Rates <API> for Latest Foreign Exchange rates$")
	public void checkApiURLIsNotNull(DataTable dataTable) {
	    
		  try {
		 
		   List<List<String>> dataTableElements = dataTable.asList(List.class);
			
		   
		   for(int i=0;i<dataTableElements.size();i++)
		   {
			   
			   fxRateDataUtility.checkAndExecuteScenario(dataTableElements.get(i).get(1).substring(1, dataTableElements.get(i).get(1).length()-1),dataTableElements.size()-1);
			   
			   fxRateDataUtility.testApiUrlIsNotNull(dataTableElements.get(i).get(0).substring(1, dataTableElements.get(i).get(0).length()-1),i);
		   
		   }
		    
		  } catch(Exception e) {
			  
			  logger.error("Received exception in checkApiURLIsNotNull function:", e);
		  }
		  
	}
	
	/**
	 * Function checks API response for URL
	 * 
	 * @param apiUrl - API URL.
	 */
	
	@When("^The \\\"([^\\\"]*)\\\" is available$")
	public void checkApiURLAvailability(String apiUrl) {
	    
		try {
	   
			fxRateDataUtility.testApiResponse(apiUrl);
	   
		} catch(Exception e) {
			  
			  logger.error("Received exception in checkApiURLAvailability function:", e);
		}
	}

	/**
	 * Function checks API response code for URL
	 * 
	 * @param responseCode - API response code.
	 */
	
	@Then("^Check success \\\"([^\\\"]*)\\\" of the response$")
	public void checkResponseStatusOfApiResponse(String responseCode) {
	   
		try {
			   
			fxRateDataUtility.testApiResponseCode(responseCode);
		
		} catch(Exception e) {
			  
			  logger.error("Received exception in checkResponseStatusOfApiResponse function:", e);
		}
	}

	/**
	 * Function checks API response schema for URL Response
	 * 
	 * @param responseSchemaFileKey - response schema file key.
	 */
	
	@Then("^Check the response \\\"([^\\\"]*)\\\"$")
	public void checkResponseSchema(String responseSchemaFileKey) {
	   
	   try {
		   
		   fxRateDataUtility.testApiResponseSchema(responseSchemaFileKey);
		   
	   } catch(Exception e) {
			  
			  logger.error("Received exception in checkResponseSchema function:", e);
	   }
	}
	
	/**
	 * Function tests After hook with tag mentioned for the corresponding feature file
	 * 
	 * Runs after scenario
	 */
	
	@After("@Smoke")
	public void tearDown() {
		
		hooksHandler.cleanUp();
		
		logger.info("Called After Scenario Hook");
	}
}
