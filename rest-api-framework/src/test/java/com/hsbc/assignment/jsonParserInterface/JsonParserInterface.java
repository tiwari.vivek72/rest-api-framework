package com.hsbc.assignment.jsonParserInterface;

import java.util.ArrayList;

/**
 * 
 * @author Vivek Tiwari
 * 
 * Content - Abstract Methods to parse Json Files
 *
 */
public interface JsonParserInterface {
	
		void parseJsonFile();
		
		String concatenateString(String url);

		String getJsonValue();

		ArrayList<?> getListOfDataItems();

		ArrayList<?> getList();
}
