package com.hsbc.assignment.restAssuredInterface;

import io.restassured.response.Response;

/**
 * 
 * @author Vivek Tiwari
 * 
 * Content - Abstract-Methods to get RestAssured Elements 
 * 
 * -- Response,Response Code, Response Body
 *
 */

public interface RestAssuredInterface {
	
	 Response getApiResponse();
	 
	 String getApiResponseCode();
	 
	 String getApiResponseBody();
	
}
