package com.hsbc.assignment.runner;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.cucumber.testng.AbstractTestNGCucumberTests;

/**
 * 
 * @author Vivek Tiwari
 * 
 * Content - AbstractTestNGCucumberTests Test Runner Class with cucumber integration.
 * 
 * Parallel Execution with Thread-Local Implementation
 *
 */
@Test
@io.cucumber.testng.CucumberOptions(features = "src/test/resources", glue = { "com.hsbc.assignment.stepDefinitions" , "com.hsbc.assignment.customReportListener"}, plugin = { "com.hsbc.assignment.customReportListener.CustomReportListener" , "pretty",
		"json:target/cucumber-reports/Cucumber.json", "junit:target/cucumber-reports/Cucumber.xml",
		"html:target/cucumber-reports" 
}, tags = { "@Smoke or @Regression" }, monochrome = true, strict = true)
public class Runner_One extends AbstractTestNGCucumberTests{
	
	/**
	 * Thread-Local object to handle thread execution
	 */
	
	private ThreadLocal<Object[][]> testRunner = new ThreadLocal<Object[][]>();
	
	/**
	 * Overriden method of AbstractTestNGCucumberTests class.
	 * 
	 * @return - 2-D Object array of scenarios.
	 */
	
	@Override
	@DataProvider
	public Object[][] scenarios() {
		
		testRunner.set(super.scenarios());
		
	    return testRunner.get();
	}
	
}