package com.hsbc.assignment.utilities;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.testng.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsbc.assignment.fxRateDataInterface.FXRateDataInterface;

/**
 * 
 * @author Vivek Tiwari
 * 
 * @Content - Utility methods to work on rest-api's
 *
 */
public final class FXRateDataUtility implements FXRateDataInterface{

	/**
	 * Logger object to log information and errors
	 */
	
	private Logger logger = Logger.getLogger(FXRateDataUtility.class);
	
	/**
	 * SearchFilesUtility Utility class object.
	 * 
	 * Calls files search functions and processing methods
	 */
	
	private SearchFilesUtility searchFilesUtility;
	
	/**
	 * RestAssuredUtility Utility class object.
	 * 
	 * Calls rest-assured related objects
	 */
	
	private RestAssuredUtility restAssuredUtility = RestAssuredUtility.getInstance();
	
	/**
	 * Static integer variable index
	 * 
	 *  Note - this variable is of significant importance
	 *  
	 *  Please do not remove
	 */
	
	public static int index;
	
	
	/**
	 * Utility method checks if API key has associated values in the data file
	 * 
	 * @param url - URL Key, index - DataTable index.
	 */
	
	@Override
	public void testApiUrlIsNotNull(String url,int index) {
	
		try
		{
		RestAssuredUtility.index=index;
		
		JsonParserUtility.getJsonParserUtility().setJsonKey(url);
		
		Assert.assertTrue(JsonParserUtility.getJsonParserUtility().getJsonValue()!=null);
		
		}
		catch(Exception e) {
			
			logger.error("Received exception in testApiUrlIsNotNull function:", e);
		}
		
	}

	/**
	 * Utility method checks API Response
	 * 
	 * @param url - API URL key.
	 */
	
	@Override
	public void testApiResponse(String url) {
	
		
		try {
			
			if(index>=JsonParserUtility.list.size())
			{
				index=0;
			}
			
			RestAssuredUtility.index=index;
				
			restAssuredUtility.setUrl(url);
			
			JsonParserUtility.getJsonParserUtility().setJsonKey(url);
			
			restAssuredUtility.setResponse(given().when().get(new URL(JsonParserUtility.getJsonParserUtility().getJsonValue())));
			
			Assert.assertNotNull(restAssuredUtility.getApiResponse().asString());
			
			index++;
			
		} catch (MalformedURLException e) {
			
			logger.error("Received exception in testApiResponse function:", e);
				
			Assert.assertFalse(true);
		}
		
	}
	
	/**
	 * Utility method checks API response code
	 * 
	 * @param responseCode - Response Code key.
	 */

	@Override
	public void testApiResponseCode(String responseCode) {
		
		try {
		
			JsonParserUtility.getJsonParserUtility().setJsonKey(responseCode);
			
			Assert.assertEquals(String.valueOf(restAssuredUtility.getApiResponse().andReturn().getStatusCode()), JsonParserUtility.getJsonParserUtility().getJsonValue());
		
		
		} catch(Exception e) {
			  
			  logger.error("Received exception in testApiResponseCode function:", e);
		}
	}

	/**
	 * Utility method checks API response body schema
	 * 
	 * @param responseBodySchemaKey - Response body schema key.
	 */
	
	@Override
	public void testApiResponseSchema(String responseBodySchemaKey) {
		
		try {
			
		searchFilesUtility = new SearchFilesUtility();
		
		JsonParserUtility.getJsonParserUtility().setJsonKey(responseBodySchemaKey);
		
		searchFilesUtility.searchDirectory(new File(System.getProperty("user.dir")), (String) ((LinkedHashMap<?,?>) JsonParserUtility.list.get(RestAssuredUtility.index)).get(JsonParserUtility.getJsonParserUtility().getJsonKey()));
		
		restAssuredUtility.getApiResponse().then().assertThat().body(matchesJsonSchema(new File(searchFilesUtility.getResult().get(0))));
		
		} catch(Exception e) {
			  
			  logger.error("Received exception in testApiResponseSchema function:", e);
			  
			  Assert.assertTrue(false);
		}
	}

	/**
	 * Utility method checks and enable correct scenario execution
	 * 
	 * @param feature - Feature Number, dataTableLength - DataTable length
	 */
	
	@Override
	public void checkAndExecuteScenario(String feature, int dataTableLength) {
		
		searchFilesUtility = new SearchFilesUtility();
				
		searchFilesUtility.searchDirectory(new File(System.getProperty("user.dir")), "api-config.json");
		
		try {
			
			JsonParserUtility.list = new ObjectMapper().readValue(new File(searchFilesUtility.getResult().get(0)), ArrayList.class);
			
			Iterator<?> iterator = ((LinkedHashMap<?,?>) JsonParserUtility.list.get(dataTableLength)).keySet().iterator();
			
			String key = "";
			
			while(iterator.hasNext()) {
				
				key=key.concat((String)iterator.next());
				
				if(key.toLowerCase().equals("feature"))
					break;
				
			}
			
			  if(!(feature.equals(((LinkedHashMap<?,?>)JsonParserUtility.list.get(index)).get(key))) && index>dataTableLength) 
				  		index=0;
			 
		} catch (Exception e) {

			logger.error("Received exception in checkAndExecuteScenario function:", e);
		}
	}
	
	/**
	 * Utility method checks API Response statusLine
	 * 
	 * @param statusLine - statusLineKey
	 */
	
	@Override
	public void checkApiResponseStatusLine(String statusLineKey) {
		
		try {
		
		searchFilesUtility = new SearchFilesUtility();
		
		JsonParserUtility.getJsonParserUtility().setJsonKey(restAssuredUtility.getUrl());
				
		restAssuredUtility.setResponse(given().when().get(new URL(JsonParserUtility.getJsonParserUtility().getJsonValue())));
		
		JsonParserUtility.getJsonParserUtility().setJsonKey(statusLineKey);
		
		Assert.assertEquals(restAssuredUtility.getApiResponse().getStatusLine(), (String) ((LinkedHashMap<?,?>) JsonParserUtility.list.get(RestAssuredUtility.index)).get(JsonParserUtility.getJsonParserUtility().getJsonKey()));
	
		} catch(Exception e) {
			
			logger.error("Received exception in checkApiResponseStatusLine function:", e);
		}
	}

	/**
	 * Utility method checks API response content-type
	 * 
	 * @param responseFormatKey - API response content type key.
	 */
	
	public void checkApiResponseContentType(String responseFormatKey) {
		
		try {
			
			searchFilesUtility = new SearchFilesUtility();
			
			JsonParserUtility.getJsonParserUtility().setJsonKey(restAssuredUtility.getUrl());
					
			restAssuredUtility.setResponse(given().when().get(new URL(JsonParserUtility.getJsonParserUtility().getJsonValue())));
			
			JsonParserUtility.getJsonParserUtility().setJsonKey(responseFormatKey);
			
			Assert.assertEquals(restAssuredUtility.getApiResponse().getContentType(), (String) ((LinkedHashMap<?,?>) JsonParserUtility.list.get(RestAssuredUtility.index)).get(JsonParserUtility.getJsonParserUtility().getJsonKey()));
		
			} catch(Exception e) {
				
				logger.error("Received exception in checkApiResponseStatusLine function:", e);
			}
	}

	/**
	 * Utiliy method checks API response header
	 * 
	 * @param responseHeaderKey - API response header key, responseHeaderContentKey - API response header content key
	 */
	
	public void checkApiResponseHeader(String responseHeaderKey, String responseHeaderContentKey) {
	
		try {
			
			searchFilesUtility = new SearchFilesUtility();
			
			JsonParserUtility.getJsonParserUtility().setJsonKey(restAssuredUtility.getUrl());
					
			restAssuredUtility.setResponse(given().when().get(new URL(JsonParserUtility.getJsonParserUtility().getJsonValue())));
			
			JsonParserUtility.getJsonParserUtility().setJsonKey(responseHeaderKey);
			
			Assert.assertTrue(restAssuredUtility.getApiResponse().getHeaders().hasHeaderWithName(JsonParserUtility.getJsonParserUtility().getJsonValue()));
			
			String responseHeaderValue = restAssuredUtility.getApiResponse().getHeader(JsonParserUtility.getJsonParserUtility().getJsonValue()).toLowerCase();
			
			JsonParserUtility.getJsonParserUtility().setJsonKey(responseHeaderContentKey);
			
			Assert.assertEquals(responseHeaderValue, (String) ((LinkedHashMap<?,?>) JsonParserUtility.list.get(RestAssuredUtility.index)).get(JsonParserUtility.getJsonParserUtility().getJsonKey()).toString().toLowerCase());
		
			} catch(Exception e) {
				
				logger.error("Received exception in checkApiResponseStatusLine function:", e);
			}
		
	}
}
