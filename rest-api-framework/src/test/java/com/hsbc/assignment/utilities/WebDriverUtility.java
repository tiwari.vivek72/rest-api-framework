package com.hsbc.assignment.utilities;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.hsbc.assignment.webDriverInterface.WebDriverInterface;

/**
 * 
 * @author Vivek Tiwari
 *
 *@Content - Web-Driver Utility Methods
 *
 * Note - Under Construction
 * 
 */

public final class WebDriverUtility implements WebDriverInterface{

	/**
	 *  Logger object to log information and errors
	 */
	   private Logger logger = Logger.getLogger(WebDriverUtility.class);
	
	   /**
		 *  WebDriverUtility object to use utility methods
		 */
	   private WebDriverUtility instance;
	   
	   /**
		 *  ThreadLocal WebDriver instance for handling threads
		 */
	   
	   private ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>()
	   {
	      @Override
	      protected WebDriver initialValue()
	      {
	         return new FirefoxDriver();
	      }
	   };
	   
	   /**
		 *  WebDriverUtility object getInstance method
		 */
	   public WebDriverUtility getInstance()
	   {
		   if(instance==null)
			   instance=new WebDriverUtility();
		   
	      return instance;
	   }

	   public WebDriverUtility() {
	
	   }

	   /**
		 *  Get WebDriver object
		 */
	   
	   public WebDriver getDriver()
	   {
	      return driver.get();
	   }

	   /**
		 *  Quit and remove WebDriver object
		 */
	   public void removeDriver()
	   {
	      driver.get().quit();
	      driver.remove();
	   }

	 /**
	  *  Get type of browser object
	  *  
	  *  @param - browserType
	  */
	   
	@Override
	public WebDriver getWebDriverType(String browserType) {
		
		try {
		
		if(browserType.equalsIgnoreCase("chrome"))
			return new ChromeDriver();
		else if(browserType.equalsIgnoreCase("firefox"))
			return new FirefoxDriver();
		else if(browserType.equalsIgnoreCase("ie"))
			return new InternetExplorerDriver();
		
		}catch(Exception e) {
			
			logger.error("Received exception in getWebDriverType function:", e);
		}
		
		return null;
	}

		
	
}
