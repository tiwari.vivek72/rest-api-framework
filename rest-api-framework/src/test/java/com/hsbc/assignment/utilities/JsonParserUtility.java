package com.hsbc.assignment.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsbc.assignment.jsonParserInterface.JsonParserInterface;

public class JsonParserUtility implements JsonParserInterface{

	/**
	 * Final logger object to log information and error.
	 */
	
	private final Logger logger = Logger.getLogger(JsonParserUtility.class);
	
	/**
	 * jsonKey object -- Key for data file
	 */
	private String jsonKey;
	
	/**
	 * jsonValue object -- Value for data file
	 */
	private String jsonValue;
	
	/**
	 * Static reference for this class
	 */
	private static JsonParserUtility jsonParserUtility = new JsonParserUtility();
	
	/**
	 * Static class object getter method
	 */
	public static JsonParserUtility getJsonParserUtility() {
		return jsonParserUtility;
	}

	/**
	 * Setter method for jsonKey
	 */
	public void setJsonKey(String jsonKey) {
		this.jsonKey = jsonKey;
	}

	/**
	 * Search files utility class object
	 */
	
	private SearchFilesUtility searchFilesUtility = new SearchFilesUtility();
	
	/**
	 * list object to store data items
	 */
	public static ArrayList<?> list;

	public JsonParserUtility(String jsonKey) {
		
		this.jsonKey=jsonKey;
	}

	public JsonParserUtility() {
	
	}
	
	/**
	 * Function - Concatenate URL components
	 * 
	 * Takes input as key.
	 */
	
	@Override
	public String concatenateString(String url) {
		
		StringBuilder stringBuilder = new StringBuilder();
		

		boolean flagOne=false, flagTwo=false;
		
		try {

	
		if(RestAssuredUtility.index>=0 && RestAssuredUtility.index<list.size())
			{
			
			Iterator<?> iterator = (Iterator<?>) ((LinkedHashMap<?,?>) list.get(RestAssuredUtility.index)).keySet().iterator();
		
			while(iterator.hasNext()) {
				
				String key = (String) iterator.next();
				
				if(!((LinkedHashMap<?,?>)list.get(RestAssuredUtility.index)).get(key).toString().isEmpty())
				{
				if(url.toLowerCase().equalsIgnoreCase(key) && key.toLowerCase().contains("basic"))
				{
					stringBuilder.append(((LinkedHashMap<?,?>)list.get(RestAssuredUtility.index)).get(key));
					
					flagOne=true;
				}
				else if(key.toLowerCase().contains("resource") && flagOne==true)
				{
					stringBuilder.append(((LinkedHashMap<?,?>)list.get(RestAssuredUtility.index)).get(key));
					
					flagTwo=true;
				}
				else if(key.toLowerCase().contains("query") && flagTwo==true)
				{
					stringBuilder.append(((LinkedHashMap<?,?>)list.get(RestAssuredUtility.index)).get(key));
				}
				else if(url.toLowerCase().equalsIgnoreCase(key) && flagOne==false && flagTwo==false){
					
					stringBuilder.append(((LinkedHashMap<?,?>)list.get(RestAssuredUtility.index)).get(key));
					
					break;
				}
				
				}
			}

			
		}
			return stringBuilder.toString();
			
		} catch (Exception e) {
			
			logger.error("Received exception in function concatenateString:", e);
		}
		
		return null;
	}
	
	/**
	 * Function - returns jsonValue as per key
	 */
	
	@Override
	public String getJsonValue() {
		
		try {
			
		parseJsonFile();
		
		return jsonValue;
		
		}catch(Exception e) {
			
			logger.error("Received exception in getJsonValue function:", e);
		}
		
		return null;
	}

	/**
	 * 
	 * @return jsonKey value
	 */
	public String getJsonKey() {
		
		return jsonKey;
	}
	
	
	/**
	 * Sets jsonValue object
	 * 
	 */
	
	@Override
	public void parseJsonFile() {
		
	
		try {
			
			getListOfDataItems();
			
			jsonValue = concatenateString(jsonKey);
		
		} catch (Exception e) {
			
			logger.error("Received exception in parseJsonFile function:", e);
		} 
		
	}

	/**
	 * Function - Returns list of data-items from api-config.json file
	 */
	
	@Override
	public ArrayList<?> getListOfDataItems() {
		
		searchFilesUtility.searchDirectory(new File(System.getProperty("user.dir")), "api-config.json");
		
		try {
			
			return list=new ObjectMapper().readValue(new File(searchFilesUtility.getResult().get(0)), ArrayList.class);
			
		} catch(Exception e) {
			
			logger.error("Received exception in function getListOfDataItems:", e);
		}
		
		return null;
	}
	
	/**
	 *  Function - returns set arrayList Object
	 */
	
	@Override
	public ArrayList<?> getList(){
		
		return list;
	}
}
