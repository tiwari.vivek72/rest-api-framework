package com.hsbc.assignment.utilities;

import org.apache.log4j.Logger;

import com.hsbc.assignment.restAssuredInterface.RestAssuredInterface;

import io.restassured.response.Response;

/**
 * 
 * @author Vivek Tiwari
 * 
 * @Content - API Response data specific methods
 * 
 * Note - Under Construction
 *
 */
public final class RestAssuredUtility implements RestAssuredInterface{

	/**
	 * Logger object to log information and errors
	 */
	
	private Logger logger = Logger.getLogger(RestAssuredUtility.class);
	
	/**
	 * response variable
	 */
	private Response response;
	
	/**
	 * responseCode variable
	 */
	private String responseCode;
	
	/**
	 * responseBody variable
	 */
	private String responseBody;

	/**
	 * static index variable
	 * 
	 * Note - Please do not delete this variable
	 */
	public static int index;
	
	/**
	 * url variable
	 */
	private String url;
	
	/**
	 * url getter method
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * url setter method
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * response setter method
	 */
	public void setResponse(Response response) {
		this.response = response;
	}

	/**
	 * responseCode setter method
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * responseBody setter method
	 */
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	/**
	 * response getter method
	 */
	@Override
	public Response getApiResponse() {
		
		return response;
	}

	/**
	 *  responseCode getter method
	 */
	@Override
	public String getApiResponseCode() {
		
		return responseCode;
	}

	/**
	 * responseBody getter method
	 */
	@Override
	public String getApiResponseBody() {
		
		return responseBody;
	}
	
	/**
	 * Method returns static object of this class
	 */
	public static RestAssuredUtility getInstance() {
		
		return new RestAssuredUtility();
	}
}
