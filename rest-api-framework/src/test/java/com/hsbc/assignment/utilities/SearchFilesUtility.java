package com.hsbc.assignment.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * @author Vivek Tiwari
 * 
 * @Content - This class contains utility methods to search a file
 *
 */
public final class SearchFilesUtility {

	 /**
	   * Logger object to log information and errors
	   * 
	   */
	
	private Logger logger = Logger.getLogger(SearchFilesUtility.class); 
	
	 /**
	   * fileNameToSearch variable
	   * 
	   */
	 
	  private String fileNameToSearch;
	  
	  /**
	   * List object to store results of searched file
	   * 
	   */
	  
	  private List<String> result = new ArrayList<String>();

	  /**
	   * Getter method to get fileNameToSearch
	   * 
	   */
	  public String getFileNameToSearch() {
		return fileNameToSearch;
	  }

	  /**
	   * Setter method to set fileNameToSearch
	   * 
	   */
	  public void setFileNameToSearch(String fileNameToSearch) {
		this.fileNameToSearch = fileNameToSearch;
	  }

	  /**
	   * Getter method to return list of found files
	   * 
	   */
	  public List<String> getResult() {
		return result;
	  }

	  /**
	   * Utility method to search directories recursively
	   * 
	   * @param directory - Directory path to search, fileNameToSearch - File to search
	   */
	  
	  public void searchDirectory(File directory, String fileNameToSearch) {

		try {
		
		setFileNameToSearch(fileNameToSearch);

		if (directory.isDirectory()) {
		    search(directory);
		} else {
			
		    logger.info(directory.getAbsoluteFile() + " is not a directory!");
		}
		
		} catch(Exception e) {
			
			logger.error("Received exception in searchDirectory function:", e);
		}

	  }

	  /**
	   * Utility method to search directories recursively
	   * 
	   * @param file - Directory path where we need to search specific file
	   */
	  private void search(File file) {

		try
		{
		if (file.isDirectory()) {

		    if (file.canRead()) {
			
		    	for (File temp : file.listFiles()) {
			    
		    		if (temp.isDirectory())
		    				search(temp);
		    		
		    		else if (getFileNameToSearch().toLowerCase().equalsIgnoreCase(temp.getName()))
		    					result.add(temp.getAbsoluteFile().toString());
		    			
		    	}

		 } else
			 logger.info(file.getAbsoluteFile() + "Permission Denied");
	      }
		} catch(Exception e) {
			
			logger.error("Received exception in search function:", e);
		}
	  }

	}
