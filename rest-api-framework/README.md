#File Updation is In-Progress

Project Title

This framework is meant to perform API/Web-Services Automation using Java, Cucumber, Rest-Assured, Test-NG, Maven and Jackson

Getting Started

1) Clone this repository using Git clone.
2) Import it to your IDE.

Running the tests

1) Go to projects pom.xml file
2) Run maven with goals "clean test site"

Please Note:

1) It is required to check if the "url under test" exists in our data files, before we proceed further (Implemented as part of @Given Step)
2) Tested with JavaSE-1.8

Framework Enhancements (Completed)

1) Framework Integration with Extent Reports

2) Jacoco Implementation (Test Coverage)

3) FindBugs Implementation (Code Quality)

Framework Enhancements (To-Do)

1) Custom Exception-Handling Class

2) Framework Code Enhancements and Formatting